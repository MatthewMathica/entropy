// Create proxy for synchronization
const contacts = function () {
    return {
        _createContact(key, alias = ''){
            return {
                alias: alias,
                key: key,
            };
        },
        async _saveContacts(contacts) {
            try {
                // Set new contacts
                await browser.storage.local.set({
                    friends: {
                        contacts: contacts
                    }
                });
                return true;
            } catch(err) {
                return false;
            }
        },
        async _getFriends() {
            const {friends} = await browser.storage.local.get('friends');
            return friends;
        },
        async addContact(key, alias = '') {
            const {contacts} = await this._getFriends();
            // If already exists
            if (contacts.some(s => s.key == key)) {
                return false;
            }
            const contact = this._createContact(key, alias);
            return await this._saveContacts([...contacts, contact]);
        },
        async removeContact(key) {
            const {contacts} = await this._getFriends();

            return await this._saveContacts(contacts.filter(s => s.key != key));
        },
        async getContacts(){
            const {contacts} = await this._getFriends();
            return contacts;
        }
    };
};
