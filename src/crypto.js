const crypto = function () {
    return {
        generateKey() {
            let key = [];
            for (let i = 0; i < 16; i++) {
                key.push(Math.round(Math.random() * 255));
            }
            return key;
        },
        _encrypt(val, key) {
            let textBytes = aesjs.utils.utf8.toBytes(val);
            let aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
            let encryptedBytes = aesCtr.encrypt(textBytes);
            return "97935F35173729858760" + aesjs.utils.hex.fromBytes(encryptedBytes);
        },
        _decrypt(val, key) {
            let textBytes = aesjs.utils.hex.toBytes(val);

            let aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
            let encryptedBytes;

            try {
                encryptedBytes = aesCtr.decrypt(textBytes);
            } catch(err) {
                alert(err);
            }

            return aesjs.utils.utf8.fromBytes(encryptedBytes);
        },
        encrypt(val, key) {
            if (Array.isArray(val)) {
                return val.map(s => this._encrypt(s, key));
            }
            return this._encrypt(val, key);
        },
        decrypt(val, key) {
            if (Array.isArray(val)) {
                return val.map(s => this._decrypt(s, key));
            }
            return this._decrypt(val, key);
        }
    };
};
