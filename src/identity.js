let baker = crypto();
let waker = contacts();

const API = {
    getAddress(vals, key) {
        return aesjs.utils.hex.fromBytes(key);
    },
    ...baker,
    ...waker
};

function onError(e) {
    console.error(e);
}

function initCrypto() {
    // Listen on encrypt/decrypt commands
    browser.storage.local.get("identity")
        .then(store =>
            {
                const [key, ] = store.identity.keys;
                browser.runtime.onMessage.addListener(
                    (data, sender) => {
                        const rpc = API[data.type].bind(API);
                        if (rpc) {
                            return Promise.resolve(rpc(data.value, key));
                        }
                        return false;
                    });
            });
}

/*
  On startup, check whether we have stored settings.
  If we don't, then store the default settings.
*/

function initStoredSettings(storedSettings) {
    if (!storedSettings.identity) {
        const identity = {
            keys: [baker.generateKey()]
        };
        const friends = {
            contacts: []
        };
        return browser.storage.local.set({identity, friends});
    }
    return Promise.resolve();
}

// Init storage keys
const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings
    .then(initStoredSettings, onError)
    .then(initCrypto, onError);
