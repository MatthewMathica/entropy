function nextPage() {

    var popupURL = browser.extension.getURL("browserAction/pages/intro.html");
    let createData = {
        type: "popup",
        url: popupURL,
        width: 800,
        height: 600
    };
    browser.windows.create(createData);
}

document.getElementById('pager').onclick = nextPage;
