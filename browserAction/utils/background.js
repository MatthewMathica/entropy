function execute(topic, data, callback) {
    browser.runtime.sendMessage({
        type: topic,
        value: data
    }).then(callback);
}
