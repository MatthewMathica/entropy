String.prototype.view = function(events = []) {
    let div = document.createElement('div');
    div.innerHTML = this.valueOf().trim();

    let dom = div.firstChild;
    for (let e in events) {
        dom[e] = events[e];
    }
    return dom;
};
