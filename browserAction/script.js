const canvas = document.getElementById("cube");
var scene = new THREE.Scene();
scene.background = null;

const aspect = canvas.height / canvas.width;
const frustumSize = 6;

let camera = new THREE.OrthographicCamera( frustumSize / - 2, frustumSize / 2, aspect * frustumSize / 2, aspect * frustumSize / - 2, 0.01, 100 );
let renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true, alpha: true});

// Light
const light = new THREE.DirectionalLight( 0xffffff, 0.2 );
scene.add(light);
light.position.set(1,2,1);

// Cube
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshPhongMaterial({color: 0xffffff});
var cube = new THREE.Mesh( geometry, material );
light.target = cube;
scene.add(cube);

// Camera
camera.position.set( 20, 20, 20 ); // all components equal
camera.lookAt( scene.position ); // or the origin
camera.zoom = 1.5;
camera.updateProjectionMatrix();

// Helpers
const clock = new THREE.Clock();

var render = function () {
    requestAnimationFrame( render );
    const delta = clock.getDelta();

    cube.rotation.y += 1 * delta;

    renderer.render(scene, camera);
};

function onWindowResize() {

    let aspect = canvas.height / canvas.width;

    camera.left = frustumSize / - 2;
    camera.right = frustumSize / 2;
    camera.top = frustumSize * aspect / 2;
    camera.bottom = - frustumSize * aspect / 2;

    camera.updateProjectionMatrix();
    renderer.setSize( canvas.width, canvas.height );

}

function setOnline(online = false) {
    let status = {
        message: 'offline',
        intensity: 0.2
    };

    if (online) {
        status.message = 'Addr';
        status.intensity = 1.0;
    }

    light.intensity = status.intensity;
    document.getElementById("network_status").innerHTML = status.message;
}

window.addEventListener('resize', onWindowResize, false );

setOnline(true);
render();
