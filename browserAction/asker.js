execute('getAddress', null, (address) => {
    document.getElementById('address').innerHTML = address;
});

const contact_bar = document.getElementById('contact_bar');
const contact_bar_button = document.getElementById('contact_bar_button');
const contact_list = document.getElementById('contact_list');

function removeContact(key, field) {
    execute('removeContact', key, success => {
        if (!success) return;
        field.remove();
    });
}

function createContactField(key) {

    const field = `
           <li key='${key}' class='list-group-item mb1'>
               <i class="fa fa-key"></i>
               ${key}
           </li>
    `.view();

    const removeButton = `
            <a href="#" class='float-right'>
                <i class="fas fa-trash"></i>
            </a>
    `.view({
        onclick: () => removeContact(key, field)
    });

    field.appendChild(removeButton);
    return field;
}

function getContacts() {
    execute('getContacts', null, contacts => {
        for (let {key} of contacts) {
            const field = createContactField(key);
            contact_list.appendChild(field);
        }
    });
}

function createContact(key) {
    execute('addContact', key, success => {
        if (!success) return;
        const field = createContactField(key);
        contact_list.appendChild(field);
    });
}

contact_bar_button.addEventListener('click', function() {
    createContact(contact_bar.value);
});

contact_bar.addEventListener('keyup', function(e){
    // Enter
    if (e.keyCode === 13) {
        e.preventDefault();
        createContact(this.value);
    }

    contact_list.childNodes.forEach(s => {
        s.hidden = false;
    });

    if (this.value != '') {
        // Search
        const hide = contact_list.querySelectorAll(`li[key]:not([key*='${this.value}'])`).forEach(s => {
            s.hidden = true;
        });
    }

});

getContacts();
