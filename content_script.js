var filter = {
    acceptNode: function(node) {
        if (/97935F35173729858760[0-9A-Fa-f]+/.test(node.data)) {
            return NodeFilter.FILTER_ACCEPT;
        }
        return NodeFilter.FILTER_SKIP;
    }
};

const prepareInputs = function(fn) {

    let i = 0;
    let inputs = document.getElementsByTagName('input');
    let textarea = document.getElementsByTagName('textarea');

    for (let j = 0; j < textarea.length; j++) {
        textarea[j].onclick = function() {
            if (this.value != '') return;
            fn(this);
        };
        textarea[j].placeholder = '🔑';
    }

    do {
        switch(inputs[i].type){
        case 'text':
        case 'search':
            inputs[i].onclick = function(options){
                if (this.value != '') return;
                fn(this);
            };
            // Key icon
            inputs[i].placeholder = '🔑';
            break;
        }
    }
    while(inputs[++i]);
};

//On Load
const decryptText = function (mutations) {

    const nodeIterator = document.createNodeIterator(document.body, NodeFilter.SHOW_TEXT, filter, false);
    let node;
    const extractCipher = /97935F35173729858760[0-9A-Fa-f]+/g;
    while ((node = nodeIterator.nextNode())) {
        // Extract values
        const withoutEmpty = node.data.replaceAll("[^a-zA-Z0-9]",'');
        let extractedSecrets = withoutEmpty.match(extractCipher) || [""];
        extractedSecrets = extractedSecrets.map(s => s.replace("97935F35173729858760", ''));
        // Except first element
        let element = node;
        // Send work to background service
        browser.runtime.sendMessage({
            type: 'decrypt',
            value: extractedSecrets
        }).then(s => {
            // Decrypted data
            let decryptedContent = document.createElement('DIV');
            decryptedContent.innerHTML = s.join(' ');
            element.parentNode.appendChild(decryptedContent);
            element.remove();
        });

    }
};

const encryptApi = (text) => {
    return browser.runtime.sendMessage({
        type: 'encrypt',
        value: text
    });
};

const doTyping = async (element, text, speed) => {
    for (let i = 0; i < text.length; i++) {
        element.value += text.charAt(i);
        await new Promise(r => setTimeout(r, speed));
    }
    return element;
};

const modal = {
    open: () => {
        return Swal.fire({
            title: 'Enter text to encrypt',
            input: 'text',
            inputLabel: 'Your secret content',
            showCancelButton: true,
            preConfirm: encryptApi,
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!';
                }
                return false;
            }
        }).then(encrypted => {
            if (encrypted.isConfirmed) {
                return encrypted.value;
            }
            throw new Error('Encryption error');
        });
    }
};

function popupInput(modal) {
    return (inputElement) => {
        modal.open()
            .then(encrypted => {
                return doTyping(inputElement, encrypted, 1);
            })
            .then(element => element.dispatchEvent(new window.Event('change', { bubbles: true })))
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Encryption failed!'
                });
            });
    };
};

prepareInputs(popupInput(modal));
// TODO: Many of mutations are useless for calling decryptText handler, could be optimized
let observer = new MutationObserver(decryptText);
const target = document.body;
observer.observe(target, {
    characterData: true,
    subtree: true,
    childList: true
});
